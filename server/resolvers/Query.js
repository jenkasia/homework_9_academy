const getMessages = async (parent, args, context) => {
  const messages = await context.prisma.messages({
    orderBy: args.orderBy
  })
  return messages
}

module.exports = {
  getMessages
}