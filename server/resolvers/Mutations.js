let messageNumber = 1

const postMessage = (parent, args, context) => {
  return context.prisma.createMessage({
    text: args.text,
    likes: 0,
    dislikes: 0
  })
}

const updateLikes = (parent, args, context) => {
  return context.prisma.updateMessage({
    where: { id: args.id },
    data: {
      likes: args.likes,
    }
  })
}

const updateDisLikes = (parent, args, context) => {
  return context.prisma.updateMessage({
    where: { id: args.id },
    data: {
      dislikes: args.dislikes,
    }
  })
}

const addReplyMessage = (parent, args, context) => {
  return context.prisma.createReply({
    text: args.text,
    likes: 0,
    dislikes: 0,
    parent: { connect: { id: args.messageId } }
  })
}

const updateReplyLikes = (parent, args, context) => {
  return context.prisma.updateReply({
    where: { id: args.id },
    data: {
      likes: args.likes,
    }
  })
}

const updateReplyDisLikes = (parent, args, context) => {
  return context.prisma.updateReply({
    where: { id: args.id },
    data: {
      dislikes: args.dislikes,
    }
  })
}


module.exports = {
  postMessage, updateLikes, updateDisLikes, addReplyMessage, updateReplyLikes,
  updateReplyDisLikes
}