const message = (parent, args, context) => {
  return context.prisma.replies({
    id: parent.id
  }).messages();
}

module.exports = {
  message
}