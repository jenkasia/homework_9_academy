const messageSubscribe = (parent, args, context) => {
  return context.prisma.$subscribe.message({
    mutation_in: ['CREATED', 'UPDATED']
  }).node();
}

const newMessage = {
  subscribe: messageSubscribe,
  resolve: payload => {
    return payload
  }
}
module.exports = {
  newMessage
}