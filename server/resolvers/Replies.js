const reply = (parent, args, context) => {
  return context.prisma.messages({
    id: parent.id
  }).replies();
}

module.exports = {
  reply
}