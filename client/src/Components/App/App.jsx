import React from 'react';
import './App.sass';
import { MainChat } from '../Chat/MainChatComponeent';

function App() {
	return (
		<div className="App">
			<MainChat />
		</div>
	);
}

export default App;
