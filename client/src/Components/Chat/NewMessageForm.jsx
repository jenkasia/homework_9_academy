import React from 'react';

export const NewMessageForm = () => {
	return (
		<form className="form__message">
			<textarea
				name="messageText"
				id="new-message__textarea"
				placeholder="Input new message"
				className="new-message__text"
			></textarea>
			<button type="submit" className="button new-message__button">
				Send
			</button>
		</form>
	);
};
