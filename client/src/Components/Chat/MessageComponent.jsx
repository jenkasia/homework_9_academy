import React from 'react';
import { ReplyMessage } from './ReplyMessageComponent';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faThumbsUp, faThumbsDown } from '@fortawesome/free-solid-svg-icons';

export const Message = props => {
	// const replies = props.replies.map(message => {
	// 	console.log('message', message);
	// 	return (
	// 		<ReplyMessage
	// 			id={message.ID}
	// 			text={message.messageText}
	// 			disLikesCount={message.disLikesCount}
	// 			likesCount={message.likesCount}
	// 		/>
	// 	);
	// });

	console.log('props', props);
	return (
		<div className="message">
			<div className="message__info">
				<div className="message__left-side">
					<p className="message__text">{props.text}</p>
					<div className="message__emotions">
						<div className="message__likes">
							<FontAwesomeIcon icon={faThumbsUp} className="message__emotion" />
							{props.likesCount}{' '}
						</div>
						<div className="message__dislikes">
							<FontAwesomeIcon icon={faThumbsDown} className="message__emotion" />
							<i class="far fa-thumbs-down"></i>
							{props.disLikesCount}{' '}
						</div>
					</div>
				</div>
				<div className="message__right-side">
					<p className="message__number"># {props.id}</p>
					<a href="/" className="message__reply button">
						Reply
					</a>
				</div>
			</div>
			{/* <div className="message__replies">{replies}</div> */}
		</div>
	);
};
