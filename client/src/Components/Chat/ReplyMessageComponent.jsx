import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faThumbsUp, faThumbsDown } from '@fortawesome/free-solid-svg-icons';

export const ReplyMessage = props => {
	console.log('props', props);
	return (
		<div className="reply-message">
			<div className="reply-message__info">
				<div className="reply-message__left-side">
					<p className="reply-message__text">{props.text}</p>
					<div className="reply-message__emotions">
						<div className="reply-message__likes">
							<FontAwesomeIcon icon={faThumbsUp} className="message__emotion" />
							{props.likesCount}{' '}
						</div>
						<div className="reply-message__dislikes">
							<FontAwesomeIcon icon={faThumbsDown} className="message__emotion" />
							{props.disLikesCount}
						</div>
					</div>
				</div>
				<div className="message__right-side">
					<p className="message__number"># {props.id}</p>
				</div>
			</div>
		</div>
	);
};
