import React, { useState } from 'react';
import { Message } from './MessageComponent';
import './chat.sass';
import { NewMessageForm } from './NewMessageForm';
import { GET_MESSAGES } from '../../Graphql/GetMessages';
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';

export const MainChat = () => {
	const { data = [], loading, error, fetchMore } = useQuery(GET_MESSAGES);
	const messages = data.getMessages ? data.getMessages : [];
	console.log('MainChat -> messages', messages);

	const messageNodes = messages.map(messageItem => {
		const messageId = messageItem.id.substring(messageItem.id.length, messageItem.id.length - 3);
		return (
			<Message
				key={messageItem.id}
				id={messageId}
				text={messageItem.text}
				likesCount={messageItem.likes}
				disLikesCount={messageItem.dislikes}
				// replies={messageItem.replies}
			/>
		);
	});

	return (
		<div className="chat">
			{messageNodes}
			<NewMessageForm />
		</div>
	);
};
